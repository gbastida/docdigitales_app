Rails.application.routes.draw do
  get 'users/new'

  root 'static_pages#home'
  get '/ayuda', to: 'static_pages#ayuda'
  get '/sobremi', to: 'static_pages#sobremi'
  get '/contacto', to: 'static_pages#contacto'
  get '/registro', to: 'users#new'
end