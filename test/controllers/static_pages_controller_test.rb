require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "DocDigitales App"
  end
  
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "Home | #{@base_title}"
  end

  test "should get help" do
    get ayuda_path
    assert_response :success
    assert_select "title", "Ayuda | #{@base_title}"
  end
  test "should get about" do
    get sobremi_path
    assert_response :success
    assert_select "title", "Sobre Mi | #{@base_title}"
  end
   test "should get contact" do
    get contacto_path
    assert_response :success
    assert_select "title", "Contacto | #{@base_title}"
  end

end
